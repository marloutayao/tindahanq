<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('categories')->insert([
        	'name' => 'SUV'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Sedan'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Sports'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Check In Joy'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Check In Sad'
        ]);


    }
}
