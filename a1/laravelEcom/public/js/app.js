let addToCartField = document.querySelectorAll('.add-to-cart-field');

if(addToCartField) {
	function isMinLimit(input) {
		if(input.value <= 0) {
			return true;
		} else {
			return false;
		}
	}

	function isMaxLimit(input) {
		if(input.value >= 99){
			return true;
		} else {
			return false;	
		}
	}
}

let dedcutQuantityBtns = document.querySelectorAll('.deduct-quantity');

dedcutQuantityBtns.forEach( (deductQuantity) => {
	let deductQuantityBtnDataID = deductQuantity.dataset.id;
	let inputTarget = document.querySelector(`input[data-id="${deductQuantityBtnDataID}"]`);

	deductQuantity.addEventListener('click', () => {
		if(!isMinLimit(inputTarget)) {
			inputTarget.value--;
		} else {
			inputTarget.value = 0;
		}
	});
});

let addQuantityBtns = document.querySelectorAll('.add-quantity');

addQuantityBtns.forEach( (addQuantity) => {
	let addQuantityBtnDataID = addQuantity.dataset.id;
	let inputTarget = document.querySelector(`input[data-id="${addQuantityBtnDataID}"]`);

	addQuantity.addEventListener('click', () => {
		if(!isMaxLimit(inputTarget)) {
			inputTarget.value++;
		} else {
			inputTarget.value = 99;
		}
	});
});

// For input fields
let inputQuantities = document.querySelectorAll('.input-quantity');
// console.log(inputQuantities);
inputQuantities.forEach((inputQuantity)=> {
	inputQuantity.addEventListener('input',function(event){
		// console.log(event);
		if(isMinLimit(inputQuantity)){
			inputQuantity.value = "";
		}
		if (isMaxLimit(inputQuantity)) {
			inputQuantity.value = 99;
		}
	});

	// Select all input on click
	inputQuantity.addEventListener('click', ()=> {
		inputQuantity.select();
	})
});