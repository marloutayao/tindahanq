<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::post('transactions/paypal/create', 'TransactionsController@paypal_store')->name('paypal.store');

Route::delete('/carts/clear', 'CartController@removeCart')->name('carts.clear');
Route::get('/', 'ProductsController@index')->name('home');

Route::resource('products', 'ProductsController');
Route::resource('categories', 'CategoriesController');
Route::resource('transactions', 'TransactionsController');
Route::resource('carts', 'CartController');
