@extends('layouts.app')
@section('content')
<script src="https://www.paypal.com/sdk/js?client-id=AU7rjgEteUm5UEAP7W0X7ZUh8KgSTPXCemx3eKcjHSl6XXLLtAms8wZkD7hRUAE0lJZ5ukHfaNjWPndI"></script>

<div class="container">
	<div class="jumbotron">
		<h3 class="text-center">My Cart</h3>
		<hr>
		@if(Session::has('product_deleted'))
		<div class="alert alert-danger">
			{{ Session::get('product_deleted')}}
		</div>
		@endif
		<div class="row">
			<div class="col-md-11 col-12">
				<form action="{{ route('carts.clear') }}" method="POST">
					@csrf
					@method("DELETE")
					<button class="btn btn-outline-danger float-right mb-2"> Clear Cart </button>
				</form>
			</div>
			<div class="col-md-10 mx-auto col-12">
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-center">
						<thead>
							<tr>
								<th>Name</th>
								<th>Unit Price</th>
								<th>Quantity</th>
								<th>Subtotal</th>
								<th>Action</th>
							</tr>
						</thead>
						@if(Session::has('cart') && !empty(Session::get('cart')))
						@foreach($products as $product)
						<tbody>
							<tr>
								<th scope="row">{{ $product->name }}</th>
								<td> &#8369 <span class="product-price">{{ number_format($product->price,2) }}</span></td>
								<td>
									<form action="{{ route('carts.store') }}" method="POST" class="add-to-cart-field">
										@csrf
										<input type="hidden" name="id" value="{{ $product->id }}">
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-prepend">
													<button type="button" class="btn btn-outline-danger deduct-quantity" data-id={{ $product->id }}> - </button>
												</div>

												<input type="number" class="form-control input-quantity" name="quantity" id="quantity" placeholder="Enter quantity" data-id={{ $product->id }} value="{{ $product->quantity }}">

												<div class="input-group-append">
													<button type="button" class="btn btn-outline-info add-quantity" data-id={{ $product->id }}> + </button>
												</div>
											</div>
											<button class="btn btn-success w-100">Update Quantity</button>
										</div>
									</form>
								</td>
								<td>
									&#8369 <span class="product-subtotal">{{ number_format($product->subtotal,2)}}</span>
								</td>
								<td>
									<form action="{{ route('carts.destroy', ['cart' => $product->id]) }}" method="POST">
										@csrf
										@method('DELETE')
										<button class="btn btn-danger btn-block"> Remove </button>
									</form>
								</td>
							</tr>
						</tbody>
						@endforeach
						<tfoot>
							<tr>
								<td colspan="3" class="text-right">Total</td>
								<td>&#8369 <span id="total_price">{{ number_format($total,2) }}</span></td>
								<input type="hidden" value="{{$total}}" id="get_total_pirce">
								<td>
									<form action="{{ route('transactions.store')}}" method="POST">
										@csrf
										<button class="btn btn-primary"> Checkout</button>
									</form>
								</td>
							</tr>
						</tfoot>
						@else
						<tr>
							<td colspan="6"><h1>NO ITEMS IN CART</h1></td>
						</tr>
						@endif
					</table>
						
				</div>
					<div class="row">
						<div class="col-md-3 col-12 offset-md-9">
							<div id="paypal-button-container"></div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>

<script>
  paypal.Buttons({
    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: document.querySelector('#get_total_pirce').value
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // Capture the funds from the transaction
      return actions.order.capture().then(function(details) {
        // Show a success message to your buyer
        // console.log(details.id)
        let csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        fetch('{{ route('paypal.store') }}', {
        	method: 'post',
        	body: JSON.stringify( { transactionNumber : details.id} ),
        	headers : {'X-CSRF-TOKEN' : csrfToken }

        }).then(response => response.json() )
        .then(result => {window.location = result.url} ) ;
      });
    }
  }).render('#paypal-button-container');
</script>

@endsection