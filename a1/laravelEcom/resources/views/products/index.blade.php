@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="jumbotron">
			<h3 class="text-center">Products</h3>
			<hr>
			@if(Session::has('product_deleted'))
				<div class="alert alert-danger">
					{{ Session::get('product_deleted')}}
				</div>
			@endif
			<div class="row">
				@foreach($products as $product)
					<div class="col-md-6 d-flex">
						<div class="card">
							<div class="card-header">
								<img class="card-img-top img-thumbnail img-fluid" src="{{ url('/public/'.$product->image) }}">
							</div>

							<div class="card-body">
								
								<h4 class="card-title mx-auto">{{ $product->category->name }}</h4>

								<strong><p class="card-text">&#8369; {{ $product->price}}</p></strong>
								<p class="card-text">{{ $product->description}}</p>
							</div>

							<div class="card-footer">
								@can('isAdmin')
								
								@else 
									<form action="{{ route('carts.store') }}" method="POST" class="add-to-cart-field">
										@csrf
										<input type="hidden" name="id" value="{{ $product->id }}">
										<div class="form-inline">
											<label for="quantity">Quantity: </label>
											<div class="input-group mb-2">
												<div class="input-group-prepend">
													<button type="button" class="btn btn-outline-danger deduct-quantity" data-id={{ $product->id }}> - </button>
												</div>

												<input type="number" value="1" class="form-control input-quantity" name="quantity" id="quantity" placeholder="Enter quantity" data-id={{ $product->id }}>

												<div class="input-group-append">
													<button type="button" class="btn btn-outline-info add-quantity" data-id={{ $product->id }}> + </button>
												</div>
											</div>
											<button class="btn btn-success btn-block mb-2">Add to Cart</button>
										</div>
									</form>
								@endcan
								
								<a href="{{ route('products.show', ['product' => $product->id] )}}" class="btn btn-outline-info btn-block mb-2">View Details</a>

								@can('isAdmin')
									<a href="{{ route('products.edit', ['product' => $product->id] )}}" class="btn btn-primary btn-block mb-2">Edit Details</a>

									<form action="{{ route('products.destroy', ['product' => $product->id] )}}" method="POST">
										@csrf
										@method('DELETE')
										<button class="btn btn-outline-danger btn-block">Delete</button>
									</form>
								@endcan
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>


@endsection