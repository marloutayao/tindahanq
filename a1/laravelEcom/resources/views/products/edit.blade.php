@extends('layouts.app')

@section('content')
	

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="jumbotron">
					<h3 class="text-center">Edit ka product??</h3>
					@if(Session::has('update_failed'))
						<div class="alert alert-warning">
							{{ Session::get('update_failed')}}
						</div>
					@endif
					@if(Session::has('update_success'))
						<div class="alert alert-success">
							{{ Session::get('update_success')}}
						</div>
					@endif
					<hr>
					<form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST" enctype="multipart/form-data">
						@csrf
						@method("PUT")
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name"> Product Name</label>
									<input type="text" name="name" id="name" class="form-control" value="{{ $product->name }}">
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label for="price"> Product Price</label>
									<input type="number" name="price" id="price" class="form-control" min="1" value="{{ $product->price }}">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="category"> Category </label>
									<select name="category" id="category" class="form-control">
										@foreach($categories as $category)
											<option value="{{ $category->id }}"
													@if($product->category_id == $category->id)
														selected
													@endif>
													{{ $category->name }}
											</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="image"> Product Image</label>
									<input type="file" name="image" id="image" class="form-control-file">
									<img src="{{ url('/public/' . $product->image) }}" class="img-fluid">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="description"> Description</label>
									<textarea name="description" id="description" class="form-control" rows="10">{{ $product->description }}</textarea>
								</div>
							</div>

							<div class="col-md-12">
								<button class="btn btn-outline-dark w-50 float-right"> Edith Mo To??</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


@endsection