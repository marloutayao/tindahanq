@extends('layouts.app')

@section('content')
	

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="jumbotron">
					<h3 class="text-center">Add New Product</h3>
					<hr>
					<form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
						@csrf
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name"> Product Name</label>
									<input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
								</div>
								@if($errors->has('name'))
								<p>
									<small class="alert alert-danger">
										@foreach($errors->get('name') as $error)
											{{ $error }}
										@endforeach
									</small>
								</p>
								@endif
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label for="price"> Product Price</label>
									<input type="number" name="price" id="price" class="form-control" min="1" value="{{ old('price') }}">
								</div>
								@if($errors->has('price'))
								<p>
									<small class="alert alert-danger">
										@foreach($errors->get('price') as $error)
											{{ $error }}
										@endforeach
									</small>
								</p>
								@endif
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="category"> Category </label>
									<select name="category" id="category" class="form-control">
										@foreach($categories as $category)
										<option value="{{ $category->id }}">{{ $category->name }}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="image"> Product Image</label>
									<input type="file" name="image" id="image" class="form-control-file">
								</div>
								@if($errors->has('image'))
								<p>
									<small class="alert alert-danger">
										@foreach($errors->get('image') as $error)
											{{ $error }}
										@endforeach
									</small>
								</p>
								@endif
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="description"> Description</label>
									<textarea name="description" id="description" class="form-control" rows="10">{{ old('description') }}</textarea>
								</div>
								@if($errors->has('description'))
								<p>
									<small class="alert alert-danger">
										@foreach($errors->get('description') as $error)
											{{ $error }}
										@endforeach
									</small>
								</p>
								@endif
							</div>

							<div class="col-md-12">
								<button class="btn btn-outline-dark w-50 float-right">Add Item</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			
			<div class="col-12 col-md-4 mx-auto">
				<h3>Add New Category</h3>
				<hr>
				@if(Session::has('category_message'))
					<div class="alert alert-success">
						{{ Session::get('category_message') }}
					</div>
				@endif
				@if($errors->has('add-category'))
					<p>
						<small class="alert alert-danger">
							@foreach($errors->get('add-category') as $error)
								{{ old('add-category') }} {{ $error }}
							@endforeach
						</small>
					</p>
				@endif
				<form action="{{ route('categories.store') }}" method="POST">
					@csrf
					<div class="form-group">
						<label for="add-category"> Add New Category </label>
						<input type="text" name="add-category" id="add-category" class="form-control">
					</div>

					<button class="btn btn-secondary float-right">Add Category</button>
				</form>
			</div>
		</div>
	</div>


@endsection