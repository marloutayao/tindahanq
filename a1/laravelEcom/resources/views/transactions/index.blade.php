@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center"> Transactions </h3>
			<hr>
		</div>
	</div>
	<div class="jumbotron">
		<div class="row">
			<div class="col-md-12 col-12 mx-auto">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<th></th>
							<th scope="col">Transaction Number</th>
							<th scope="col">Date</th>
							<th scope="col">Mode of Payment</th>
							<th scope="col">Total</th>
							<th scope="col">Status</th>
							@can('isAdmin')
								<th scope="col">Change Status</th>
							@endcan
						</thead>
						<tbody>
							@foreach($transactions as $transaction)
							<tr
							@if(Session::has('status_updated') && Session::get('status_updated') == $transaction->id)
								class=bg-light		
							@endif
							>
								<td>
									<a href="{{ route('transactions.show', ['transaction' => $transaction->id])}}" class="btn btn-primary">View Details</a>
								</td>
								<td>
									<strong>{{ $transaction->transaction_number }}</strong>
								</td>
								<td>{{ $transaction->created_at->format('F m, Y - h:i:s') }}</td>
								<td>{{ $transaction->payment_mode->name }}</td>
								<td>&#8369; {{ number_format($transaction->total, 2) }}</td>
								@if($transaction->status->name == 'pending')
								<td>
									<span class="badge badge-warning">{{ strtoupper($transaction->status->name) }}
									</span>
									
								</td>
								@elseif($transaction->status->name == 'processing')
								<td>
									<span class="badge badge-info">{{ strtoupper($transaction->status->name) }}
									</span>
								</td>
								@else
								<td class="text-center">
									<span class="badge badge-success">{{ strtoupper($transaction->status->name) }}</span>
								</td>
								@endif
								@can('isAdmin')
									<td>
										<form action="{{ route('transactions.update', ['transaction' => $transaction->id])}}" method="POST">
											@csrf
											@method("PUT")
											<select class="custom-select mb-1" name="status" id="edit-transaction-{{$transaction->id}}">
												@foreach($statuses as $status)
												<option value="{{ $status->id }}"
													{{$transaction->status->id == $status->id ? 'selected' : ""}}>
													{{ strtoupper($status->name) }}
												</option>
												@endforeach
											</select>
											<button class="btn btn-info w-100">Change Status</button>
										</form>
									</td>
								@endcan
							</tr>
							@endforeach
						</tbody>
{{-- 							<tr>
								<td class="text-right" scope="row" colspan="4"><strong>Total</strong></td>
								<td>&#8369; 12312321</td>
							</tr> --}}
						</table>
						{{ $transactions->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection