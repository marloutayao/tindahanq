@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center"> Transaction Details </h3>
				<hr>
			</div>
		</div>

		<div class="jumbotron">
			<div class="row">
				<div class="col-md-8 col-12 mx-auto">
					<div class="table-responsive">
						<table class="table table-sm table-borderless">
							<tbody>
								<tr>
									<td>Customer Name:</td>
									<td><strong>{{ $transaction->user->name }}</strong></td>
								</tr>
								<tr>
									<td>Transaction number: </td>
									<td><strong>{{ $transaction->transaction_number}}</strong></td>
								</tr>
								<tr>
									<td>Mode of Payment</td>
									<td>{{ $transaction->payment_mode->name }}</td>
								</tr>
								<tr>
									<td>Status</td>
									<td>{{ $transaction->status->name }}</td>
								</tr>
								<tr>
									<td>Date</td>
									<td>{{ $transaction->created_at->format('F d, Y') }}</td>
								</tr>
							</tbody>
						</table>

						<table class="table table-bordered">
							<thead>
								<th scope="col">Name</th>
								<th scope="col">Unit Price</th>
								<th scope="col">Quantity</th>
								<th scope="col">Amount</th>
							</thead>
							<tbody>
								@foreach($transaction->products as $product)
								<tr>
									<td>{{ $product->name }}</td>
									<td>&#8369; <span>{{ number_format($product->price, 2) }}</span></td>
									<td>{{ $product->pivot->quantity }}</td>
									<td>&#8369; <span>{{ number_format($product->pivot->subtotal, 2) }}</span></td>
								</tr>
								@endforeach
							</tbody>
							<tr>
								<td class="text-right" scope="row" colspan="3"><strong>Total</strong></td>
								<td>&#8369; {{ number_format($transaction->total, 2) }}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection