<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Category;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $products = $product->all();
        return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $this->authorize('create', Product::class);

        $category = $category->all();
        // $category = Category::all();
        return view('products.create')->with('categories', $category);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|min:4',
            'price' => 'required|numeric|digits_between:1,8',
            'category' => 'required',
            'image' => 'required|image|max:4000|mimes:jpeg,jpg,png,gif',
            'description' => 'required|string',
        ]);

        $file = $request->file('image');
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file_extension = $file->extension();
        
        $random_name = Str::random(10);

        $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_name . "_" . $file_name . "." . $file_extension;

        $filepath = $file->storeAs('images', $new_file_name, 'public');
        
        // dd($new_file_name);

        $name = $request->input('name');
        $price = $request->input('price');
        $category_id = $request->input('category');
        $description = $request->input('description');
        $image = $filepath;

        $product = new Product;

        $product->name = $name;
        $product->price = $price;
        $product->category_id = $category_id;
        $product->description = $description;
        $product->image = $image;

        $product->save();

        return redirect( route('products.show', ['product' => $product->id] ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('update', $product);
        
        $category = Category::all();
        return view('products.edit')
            ->with('product', $product)
            ->with('categories', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize('update', $product);
         $request->validate([
            'name' => 'required|string|min:4',
            'price' => 'required|numeric|digits_between:1,8',
            'category' => 'required',
            // 'image' => 'required|image|max:4000|mimes:jpeg,jpg,png,gif',
            'description' => 'required|string',
        ]);

        if(
            !$request->hasFile('image') &&
            $product->name == $request->input('name') &&
            $product->description == $request->input('description') &&
            $product->price == $request->input('price') &&
            $product->category_id == $request->input('category')
        ) {
            $request->session()->flash('update_failed', 'Wala kang binago eh???');

            // return redirect( route('products.edit', ['product' => $product->id]));
        } else {
            // echo "awit";

            if($request->hasFile('image')) {
                $request->validate([
                    'image' => 'required|image|max:4000|mimes:jpeg,jpg,png,gif',
                ]);

                $file = $request->file('image');
                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file_extension = $file->extension();
                
                $random_name = Str::random(10);

                $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_name . "_" . $file_name . "." . $file_extension;

                $filepath = $file->storeAs('images', $new_file_name, 'public');

                $product->image = $filepath;
            }

            $product->name = $request->input('name');
            $product->price = $request->input('price');
            $product->description = $request->input('description');
            $product->category_id = $request->input('category');
            
            $product->save();

            $request->session()->flash('update_success', 'Congrats, updated na ang iyong product');
        }
            return redirect( route('products.edit', ['product' => $product->id]));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);
        $product->delete();
        // $request->session()->flash('product_deleted', 'AWIT, na delete na nga!!');
        return redirect(route('products.index'))->with('product_deleted', 'AWIT nga, na delete na nga?!');
    }
}
