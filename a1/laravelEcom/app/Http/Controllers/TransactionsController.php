<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Session;
use Auth;
use Str;
use App\Product;
use Illuminate\Support\Facades\DB;
use App\Status;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
        if(Auth::user()->role->name == 'admin'){
            $transact = $transaction::paginate(5);
        } else {
            $transact = $transaction::where('user_id', '=' , Auth::user()->id)->paginate(5);
        }
        
        $statuses = Status::all();
        
        // $transactions = $transaction::all()->simplePaginate(5);
        return view('transactions.index')
            ->with('transactions', $transact)
            ->with('statuses', $statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total = 0;

        $transaction = new Transaction;
            $user_id = Auth::user()->id;
            $transaction_number = $user_id . Str::random(13) . time();
        
        $transaction->transaction_number = $transaction_number;
        $transaction->user_id = $user_id;

        $transaction->save();

        $cart_ids = array_keys(Session::get('cart'));
        $products = Product::find($cart_ids);

        // dd($products);
        foreach (Session::get('cart') as $cart_order_id => $quantity) {
            foreach($products as $product) {
                if($product->id == $cart_order_id) {
                    $transaction->products()->attach(
                        $product->id,
                        ['quantity' => $quantity,
                         'price' => $product->price,
                         'subtotal' => $product->price * $quantity,
                        ]
                    );
                }
            }
        }

        $transaction_products = $transaction->products;
        foreach($transaction_products as $transaction_product) {
            $total += $transaction_product->pivot->subtotal;
        }

        $transaction->total = $total;
        $transaction->save();

        Session::forget('cart');

        return redirect( route('transactions.show', ['transaction' => $transaction->id] ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $this->authorize('view', $transaction);
        
        return view('transactions.show', ['transaction' => $transaction]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $transaction->status_id = $request->input('status');
        $transaction->save();

        return redirect(route('transactions.index'))
            ->with('status_updated', $transaction->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }



    public function paypal_store(Request $request) {
        
        $data = json_decode($request->getContent(), true);

        // Crate new entry in the transaction table 
        $transaction = new Transaction;
        $user_id = Auth::user()->id;
        // $transaction_number = $user_id . Str::random(13) . time();
        $transaction_number = $data['transactionNumber'];
        
        $transaction->transaction_number = $transaction_number;
        $transaction->user_id = $user_id;
        $transaction->payment_mode_id = 2;
        $transaction->save();

        $cart_ids = array_keys(Session::get('cart'));
        $products = Product::find($cart_ids);

        // dd($products);
        foreach (Session::get('cart') as $cart_order_id => $quantity) {
            foreach($products as $product) {
                if($product->id == $cart_order_id) {
                    $transaction->products()->attach(
                        $product->id,
                        ['quantity' => $quantity,
                         'price' => $product->price,
                         'subtotal' => $product->price * $quantity,
                        ]
                    );
                }
            }
        }

        $total = 0;
        $transaction_products = $transaction->products;
        foreach($transaction_products as $transaction_product) {
            $total += $transaction_product->pivot->subtotal;
        }

        $transaction->total = $total;
        $transaction->save();

        Session::forget('cart');

        return ["url" => route('transactions.show', ['transaction' => $transaction->id])];
    }
}
