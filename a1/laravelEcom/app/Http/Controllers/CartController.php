<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;
use Auth;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::all();

        $products = [];
        $total = 0;

        if(Session::has('cart')) {
            $product_ids = array_keys(Session::get('cart'));

            $products = Product::find($product_ids);
            foreach($products as $product) {
                $product['quantity'] = Session::get("cart.$product->id");
                $product['subtotal'] = $product['price'] * $product['quantity'];

                $total += $product['subtotal'];
            }
            return view('carts.index')->with('products', $products)->with('total', $total);
        } else {
            return view('carts.index');
        }

        // return view('carts.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()) {
            $request->validate([
                'id' => 'required',
                'quantity' => 'required|numeric|min:1|max:99'
            ]);

            $product_quantity = $request->input('quantity');
            $id = $request->input('id');

        // $add_to_cart = [$id => $product_quantity];

        // $request->session()->put("cart", [$add_to_cart]);
            $request->session()->put("cart.$id", $product_quantity);
            return redirect( route('carts.index') );
        } else {
            return redirect(route('login'))->with('login_first', 'You must login first to buy item, Thank you!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $cart )
    {
        // $try = Session::get('cart');
        // print_r($try[$cart]);
        // $request->session()->forget($try);
        $request->session()->forget("cart.".$cart);

        return redirect(route('carts.index'));
    }

    public function removeCart() {
        Session::forget("cart");
        return redirect(route('carts.index'));
    }
}
