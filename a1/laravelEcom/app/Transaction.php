<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    public function status(){
    	return $this->belongsTo('App\Status');
    }
    
    public function payment_mode(){
    	return $this->belongsTo('App\Payment_mode');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    // many to many relation ship
    public function products(){
        return $this->belongsToMany('App\Product', 'transaction_product')
            ->withPivot('price', 'quantity', 'subtotal')
            ->withTimestamps();
    }

}
