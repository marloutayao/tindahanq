@extends('layouts.app')

	@section('title')
		Index
	@endsection

	@section('content')
	
		<div class="jumbotron">
			<h1>{{ $title }}</h1>
			<p>This website is created using Laravel</p>
		</div>

	@endsection