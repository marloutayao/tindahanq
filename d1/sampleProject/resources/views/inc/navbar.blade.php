<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
  <a class="navbar-brand" href="/">LARABEL</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#burgerMoto" aria-controls="burgerMoto">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="burgerMoto">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('about') }}">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('services') }}">Services</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('posts.index') }}">View All Posts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('posts.create') }}">Create</a>
      </li>
    </ul>
  </div>
</nav>