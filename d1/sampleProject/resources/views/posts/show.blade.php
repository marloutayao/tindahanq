@extends('layouts.app')

	@section('title')
		View Post
	@endsection

	@section('content')
	
		<div class="jumbotron">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						@if($post)
							<div class="card">
								<div class="card-header">
									<h1>{{ $post->title }}</h1>
								</div>
								<div class="card-body">
									<h4>{{ $post->body }}</h4>
								</div>
								<div class="card-footer">
									<a class="btn btn-info btn-block" href="{{ route('posts.edit', ['post' => $post->id] ) }}"> Edithh????</a>
								</div>
							</div>
						@else
							<div class="card">
								<div class="card-header">
									<h1>No post</h1>
								</div>
								<div class="card-body">
									<h4>Found</h4>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>			
		</div>

	@endsection