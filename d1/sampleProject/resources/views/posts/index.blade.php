@extends('layouts.app')

	@section('title')
		Post Index
	@endsection

	@section('content')
	
		<div class="jumbotron">
			<div class="container">
				<div class="row">
					@if(count($posts) > 0)
					@foreach($posts as $post)
						<div class="col-md-6 py-2">
							<div class="card shadow">
								<div class="card-header">
									<a href="{{ route('posts.show', ['post' => $post->id ]) }}"><h1>{{$post->title}}</h1></a>
								</div>
								<div class="card-body">
									<h4>{{$post->body}}</h4>
									<a href="{{ route('posts.edit', ['post' => $post->id]) }}" class="btn btn-info float-right px-5"> Edit </a>
									
									<form action="{{ route('posts.destroy', ['post' => $post->id]) }}" method="POST">
										@csrf
										@method('DELETE')

										<button class="btn btn-danger"> REMOVE </button>
									</form>
								</div>
							</div>
						</div>
					@endforeach
					@else
						<div class="card">
							<div class="card-header">
								<h1>No post</h1>
							</div>
							<div class="card-body">
								<h4>Found</h4>
							</div>
						</div>
					@endif
				</div>
			</div>			
		</div>

	@endsection