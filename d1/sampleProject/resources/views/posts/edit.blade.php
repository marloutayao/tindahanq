@extends('layouts.app')

	@section('title')
		Edit Post
	@endsection

	@section('content')
	
		<div class="jumbotron">
			<h1> EDIT KA SIR ??? </h1>
			<div class="container">
				<div class="row">
					<div class="col-md-8 mx-auto col-12">
						<form method="POST" action="{{ route('posts.update', ['post' => $post->id]) }}">
							@csrf
							@method('PUT')
							<div class="form-group">
								<label for="title"> Title </label>
								<input type="text" name="title" class="form-control" value="{{ $post->title }}"> 
							</div>
							<div class="form-group">
								<label for="body"> Body </label>
								<textarea name="body" class="form-control" rows="8">{{ $post->body }}</textarea>
							</div>
							<button class="btn btn-outline-dark float-right px-5"> Edit Post </button>
						</form>
					</div>
				</div>
			</div>			
		</div>

	@endsection