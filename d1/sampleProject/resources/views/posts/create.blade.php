@extends('layouts.app')

	@section('title')
		Create Post
	@endsection

	@section('content')
	
		<div class="jumbotron">
			<h1> CREATE KA SIR ??? </h1>
			<div class="container">
				<div class="row">
					<div class="col-md-8 mx-auto col-12">
						@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						<form method="POST" action="{{ route('posts.store') }}">
							@csrf
							<div class="form-group">
								<label for="title"> Title </label>
								<input type="text" name="title" class="form-control" value="{{ old('title') }}"> 
							</div>
							<div class="form-group">
								<label for="body"> Body </label>
								<textarea name="body" class="form-control" rows="8">{{ old('body') }}</textarea>
							</div>
							<button class="btn btn-outline-dark float-right px-5"> Add Post </button>
						</form>
					</div>
				</div>
			</div>			
		</div>

	@endsection