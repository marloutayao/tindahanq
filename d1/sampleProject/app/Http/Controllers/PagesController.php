<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        $title = 'Welcome to Larabel';
        return view('pages.index')->with('title',$title);   
    }

    public function about() {
        $title = 'About Page';
        return view('pages.about', ['title' => $title]); 
    }

    public function services() {
        $data = [
            'title' => 'Services Page',
            'services' => [
                'Web Design',
                'Development',
                'SEO'
            ]
        ];
        return view('pages.services', $data);  
    }
}
